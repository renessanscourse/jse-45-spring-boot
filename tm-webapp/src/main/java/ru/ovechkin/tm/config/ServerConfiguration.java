package ru.ovechkin.tm.config;

import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.WebApplicationInitializer;
import ru.ovechkin.tm.soap.endpoint.AuthenticationEndpoint;
import ru.ovechkin.tm.soap.endpoint.ProjectEndpoint;
import ru.ovechkin.tm.soap.endpoint.TaskEndpoint;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;
import javax.sql.DataSource;
import javax.xml.ws.Endpoint;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = "ru.ovechkin.tm")
@EnableJpaRepositories("ru.ovechkin.tm.repository")
@PropertySource("classpath:application.properties")
public class ServerConfiguration implements WebApplicationInitializer {

    @Bean
    public DataSource dataSource(
            @Value("${db.driver}") final String driver,
            @Value("${db.login}") final String userName,
            @Value("${db.password}") final String password,
            @Value("${db.url}") final String url
    ) {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(driver);
        dataSource.setUrl(url);
        dataSource.setUsername(userName);
        dataSource.setPassword(password);
        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            @NotNull final DataSource dataSource,
            @Value("${db.dialect}") final String dialect
    ) {
        final LocalContainerEntityManagerFactoryBean factoryBean;
        factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.ovechkin.tm.entity");
        final Properties properties = new Properties();
        properties.put("hibernate.show_sql", "true");
        properties.put("hibernate.hbm2ddl.auto", "update");
        properties.put("hibernate.dialect", dialect);
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    @Bean
    public PlatformTransactionManager transactionManager(
            @NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory
    ) {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

    @Bean
    public SpringBus cxf() {
        return new SpringBus();
    }

//    @Bean
//    public Endpoint projectEndpointRegistry(final ProjectEndpoint projectEndpoint) {
//        final EndpointImpl endpoint = new EndpointImpl(cxf(), projectEndpoint);
//        endpoint.publish("/ProjectEndpoint");
//        return endpoint;
//    }
//
//    @Bean
//    public Endpoint taskEndpointRegistry(final TaskEndpoint taskEndpoint) {
//        final EndpointImpl endpoint = new EndpointImpl(cxf(), taskEndpoint);
//        endpoint.publish("/TaskEndpoint");
//        return endpoint;
//    }
//
//    @Bean
//    public Endpoint authenticationEndpointRegistry(final AuthenticationEndpoint authenticationEndpoint) {
//        final EndpointImpl endpoint = new EndpointImpl(cxf(), authenticationEndpoint);
//        endpoint.publish("/AuthenticationEndpoint");
//        return endpoint;
//    }

    @Override
    public void onStartup(ServletContext servletContext) {
        final CXFServlet cxfServlet = new CXFServlet();
        final ServletRegistration.Dynamic dynamicCXF = servletContext.addServlet("cxfServlet", cxfServlet);
        dynamicCXF.addMapping("/ws/*");
        dynamicCXF.setLoadOnStartup(1);
    }

}