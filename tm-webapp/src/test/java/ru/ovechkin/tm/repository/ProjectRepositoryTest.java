package ru.ovechkin.tm.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.ovechkin.tm.config.WebMvcConfig;
import ru.ovechkin.tm.entity.Project;

import java.util.Optional;

import static org.junit.Assert.*;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebMvcConfig.class)
public class ProjectRepositoryTest {

    @Autowired
    private ProjectRepository projectRepository;

    @Test
    public void testSave() {
        final Project project = new Project();
        project.setName("TEST_SAVE");
        projectRepository.save(project);
        assertNotNull(projectRepository.findById(project.getId()));
    }

    @Test
    public void testFindById() {
        final Project project = new Project();
        project.setName("TEST_FIND_ID");
        projectRepository.save(project);
        assertEquals(project.toString(), projectRepository.findById(project.getId()).get().toString());
    }

    @Test
    public void testDeleteById() {
        final Project project = new Project();
        project.setName("TEST_DELETE");
        projectRepository.save(project);
        assertNotNull(projectRepository.findById(project.getId()));
        projectRepository.deleteById(project.getId());
        assertEquals(Optional.empty(), projectRepository.findById(project.getId()));
    }

    @Test
    public void testUpdate() {
        final Project project = new Project();
        project.setName("TEST_TO_CHANGE");
        projectRepository.save(project);
        assertEquals(project.toString(), projectRepository.findById(project.getId()).get().toString());
        project.setName("TEST_CHANGED");
        projectRepository.save(project);
        assertEquals(project.toString(), projectRepository.findById(project.getId()).get().toString());
    }

}