package ru.ovechkin.tm.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.ovechkin.tm.config.WebMvcConfig;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.entity.Task;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumerated.Role;
import ru.ovechkin.tm.repository.ProjectRepository;
import ru.ovechkin.tm.repository.TaskRepository;
import ru.ovechkin.tm.repository.UserRepository;
import ru.ovechkin.tm.util.UserUtil;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebMvcConfig.class)
public class TaskControllerTest {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @Lazy
    @Autowired
    private PasswordEncoder passwordEncoder;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc =
                MockMvcBuilders
                        .webAppContextSetup(this.webApplicationContext)
                        .build();
        final User user = new User();
        user.setId("constantId");
        user.setLogin("test_user");
        user.setPasswordHash(passwordEncoder.encode("test_user"));
        user.setRole(Role.USER);
        userRepository.save(user);
        final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("test_user", "test_user");
        final Authentication authentication =
                authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @Test
    public void testShow() throws Exception {
        final Project project = new Project();
        project.setName("testShowTasks");
        project.setDescription("testShowTasks");
        project.setUser(userRepository.findByLogin(UserUtil.getUser().getUsername()));
        projectRepository.save(project);
        this.mockMvc
                .perform(MockMvcRequestBuilders.get("/tasks/" + project.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(view().name("tasks/all"));
    }

    @Test
    public void testGetCreateTaskForm() throws Exception {
        Project project = new Project();
        project.setName("testGetCreateTaskForm");
        project.setDescription("testGetCreateTaskForm");
        project.setUser(userRepository.findByLogin(UserUtil.getUser().getUsername()));
        this.mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/tasks/createForm")
                        .param("projectId", project.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(view().name("tasks/create"));
    }

    @Test
    public void testCreate() throws Exception {
        final Project project = new Project();
        project.setName("testCreate");
        project.setDescription("testCreate");
        project.setUser(userRepository.findByLogin(UserUtil.getUser().getUsername()));
        projectRepository.save(project);
        final Task task = new Task();
        task.setName("testTaskCreate");
        task.setDescription("testTaskCreate");
        task.setProject(project);
        task.setUser(project.getUser());
        Assert.assertFalse(taskRepository.findById(task.getId()).isPresent());
        this.mockMvc
                .perform(MockMvcRequestBuilders
                        .post("/tasks/create")
                        .param("projectId", project.getId())
                        .flashAttr("task", task))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(view().name("redirect:/tasks/" + project.getId()));
        Assert.assertTrue(taskRepository.findById(task.getId()).isPresent());
    }

    @Test
    public void testRemove() throws Exception {
        final Project project = new Project();
        project.setName("testRemove");
        project.setDescription("testRemove");
        project.setUser(userRepository.findByLogin(UserUtil.getUser().getUsername()));
        projectRepository.save(project);
        final Task task = new Task();
        task.setName("testRemoveTask");
        task.setDescription("testRemoveTask");
        task.setProject(project);
        task.setUser(project.getUser());
        taskRepository.save(task);
        Assert.assertTrue(taskRepository.findById(task.getId()).isPresent());
        this.mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/tasks/remove")
                        .param("taskId", task.getId())
                        .param("projectId", project.getId()))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(view().name("redirect:/tasks/" + project.getId()));
        Assert.assertFalse(taskRepository.findById(task.getId()).isPresent());
    }

    @Test
    public void testGetEditForm() throws Exception {
        final Project project = new Project();
        project.setName("testGetEditForm");
        project.setDescription("testGetEditForm");
        project.setUser(userRepository.findByLogin(UserUtil.getUser().getUsername()));
        projectRepository.save(project);
        final Task task = new Task();
        task.setName("testGetEditForm");
        task.setDescription("testGetEditForm");
        task.setProject(project);
        task.setUser(project.getUser());
        taskRepository.save(task);
        this.mockMvc
                .perform(MockMvcRequestBuilders.get("/tasks/editForm/" + task.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(view().name("/tasks/edit"));
    }

    @Test
    public void testEdit() throws Exception {
        final Project project = new Project();
        project.setName("testEdit");
        project.setDescription("testEdit");
        project.setUser(userRepository.findByLogin(UserUtil.getUser().getUsername()));
        projectRepository.save(project);
        final Task task = new Task();
        task.setName("testEdit");
        task.setDescription("testEdit");
        task.setProject(project);
        task.setUser(userRepository.findByLogin(UserUtil.getUser().getUsername()));
        taskRepository.save(task);
        Assert.assertTrue(taskRepository.findById(task.getId()).isPresent());
        final Task taskEdited = new Task();
        taskEdited.setName("EDITED");
        taskEdited.setDescription("EDITED");
        taskEdited.setProject(project);
        taskEdited.setUser(userRepository.findByLogin(UserUtil.getUser().getUsername()));
        this.mockMvc
                .perform(MockMvcRequestBuilders
                        .post("/tasks/edit")
                        .param("taskId", task.getId())
                        .param("projectId", project.getId())
                        .flashAttr("task", taskEdited))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(view().name("redirect:/tasks/" + project.getId()));
        Assert.assertEquals(taskEdited.toString(), taskRepository.findById(task.getId()).get().toString());
    }

}